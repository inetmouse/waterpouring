package com.pathfinder;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class VerticesTest {
    final int maxVolX = 5;
    final int maxVolY = 3;

    @Test
    public void test_fillUpX() {
        final Vertices ver = createVertices(0, 2);
        final Vertices expected = createVertices(5, 2);
        assertEquals(expected, ver.fillUpX());
    }

    @Test
    public void test_fillUpY() {
        final Vertices ver = createVertices(0, 1);
        final Vertices expected = createVertices(0, 3);
        assertEquals(expected, ver.fillUpY());
    }

    @Test
    public void test_emptyX() {
        final Vertices ver = createVertices(5, 1);
        final Vertices expected = createVertices(0, 1);
        assertEquals(expected, ver.emptyX());
    }

    @Test
    public void test_emptyY() {
        final Vertices ver = createVertices(5, 3);
        final Vertices expected = createVertices(5, 0);
        assertEquals(expected, ver.emptyY());
    }

    @Test
    public void test_pourXY() {
        final Vertices ver = createVertices(5, 1);
        final Vertices expected = createVertices(3, 3);
        assertEquals(expected, ver.pourXY());
    }

    @Test
    public void test_pourYX() {
        final Vertices ver = createVertices(2, 3);
        final Vertices expected = createVertices(5, 0);
        assertEquals(expected, ver.pourYX());
    }

    @Test
    public void test_isConnected() {
        final Vertices ver = createVertices(2, 3);
        final Vertices connectedVer1 = createVertices(5, 0);
        final Vertices connectedVer2 = createVertices(0, 3);
        final Vertices connectedVer3 = createVertices(2, 0);

        final Vertices notConnectedVer1 = createVertices(4, 0);
        final Vertices notConnectedVer2 = createVertices(5, 1);

        assertTrue(ver.isConnected(connectedVer1));
        assertTrue(ver.isConnected(connectedVer2));
        assertTrue(ver.isConnected(connectedVer3));

        assertFalse(ver.isConnected(notConnectedVer1));
        assertFalse(ver.isConnected(notConnectedVer2));
    }

    private Vertices createVertices(final int volX, final int volY) {
        return new Vertices(volX, volY, maxVolX, maxVolY);
    }
}
