package com.pathfinder;

public enum Operation {
    FILL_UP_X {
        @Override
        public String getInstruction() {
            return " fill up first bottle";
        };
    },
    FILL_UP_Y {
        @Override
        public String getInstruction() {
            return " fill up second bottle";
        };
    },
    EMPTIFY_X {
        @Override
        public String getInstruction() {
            return " emptify first bottle";
        };
    },
    EMPTIFY_Y {
        @Override
        public String getInstruction() {
            return " emptify second bottle";
        };
    },
    POUR_XY{
        @Override
        public String getInstruction() {
            return " pour water from first to second bottle unless either second bottle is full or first is empty";
        };
    },
    POUR_YX{
        @Override
        public String getInstruction() {
            return " pour water from second to first bottle unless either first bottles is full or second is empty";
        };
    },;

    public abstract String getInstruction();
}
