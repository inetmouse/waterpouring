package com.pathfinder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;

class Vertices implements Comparable<Vertices> {

    private final int maxVolX;
    private final int maxVolY;
    private final int volX;
    private final int volY;

    private Vertices previous;

    private double minDistance = Double.POSITIVE_INFINITY;
    private List<Vertices> adjacency;

    Vertices(int volX, int volY, int maxVolX, int maxVolY) {
        this.maxVolX = maxVolX;
        this.maxVolY = maxVolY;
        this.volX = volX;
        this.volY = volY;
    }

    public int getVolX() {
        return volX;
    }

    public int getVolY() {
        return volY;
    }

    public List<Vertices> getAdjacency() {
        return adjacency;
    }

    public void setAdjacency(List<Vertices> adjacency) {
        this.adjacency = adjacency;
    }

    public double getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    public Vertices fillUpX() {
        return new Vertices(maxVolX, volY, maxVolX, maxVolY);
    }

    public Vertices fillUpY() {
        return new Vertices(volX, maxVolY, maxVolX, maxVolY);
    }

    public Vertices emptyX() {
        return new Vertices(0, volY, maxVolX, maxVolY);
    }

    public Vertices emptyY() {
        return new Vertices(volX, 0, maxVolX, maxVolY);
    }

    public Vertices pourXY() {
        return new Vertices(volX - qntToPourXY(), volY + qntToPourXY(), maxVolX, maxVolY);
    }

    public Vertices pourYX() {
        return new Vertices(volX + qnyToPourYX(), volY - qnyToPourYX(), maxVolX, maxVolY);
    }

    public boolean isConnected(Vertices vertices) {
        if (this.equals(vertices)) {
            return false;
        }

        return vertices.equals(this.fillUpX())
                || vertices.equals(this.fillUpY())
                || vertices.equals(this.emptyX())
                || vertices.equals(this.emptyY())
                || vertices.equals(this.pourXY())
                || vertices.equals(this.pourYX());
    }

    private int qntToPourXY() {
        return (volX + volY) > maxVolY ? maxVolY - volY : volX;
    }

    private int qnyToPourYX() {
        return (volY + volX) > maxVolX ? maxVolX - volX : volY;
    }

    public int compareTo(Vertices other) {
        return Double.compare(minDistance, other.minDistance);
    }

    public Vertices getPrevious() {
        return previous;
    }

    public void setPrevious(Vertices previous) {
        this.previous = previous;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertices other = (Vertices) obj;
        return new EqualsBuilder()
                .append(volX, other.volX)
                .append(volY, other.volY)
                .append(maxVolX, other.maxVolX)
                .append(maxVolY, other.maxVolY)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(volX)
                .append(volY)
                .append(maxVolX)
                .append(maxVolY)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "(" + volX + ";" + volY + ")";
    }
}
