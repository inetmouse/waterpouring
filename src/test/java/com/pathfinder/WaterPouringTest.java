package com.pathfinder;

import com.pathfinder.WaterPouring.ParamsAsker;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static java.util.Arrays.asList;
import static com.pathfinder.WaterPouring.vertices;

public class WaterPouringTest {
    ParamsAsker asker;
    int firstBottleCapacity;
    int secondBottleCapacity;
    int desiredAmount;

    @Before
    public void setUp() {
        asker = mock(ParamsAsker.class);
    }

    @Test
    public void test_3_5_bottles_4_liters_desired() {
        firstBottleCapacity = 5;
        secondBottleCapacity = 3;
        desiredAmount = 1;

        when(asker.ask(WaterPouring.ENTER_FIRST_BOTTLE_VOL)).thenReturn(firstBottleCapacity);
        when(asker.ask(WaterPouring.ENTER_SECOND_BOTTLE_VOL)).thenReturn(secondBottleCapacity);
        when(asker.ask(WaterPouring.ENTER_DESIRED_TARGET_AMOUNT)).thenReturn(desiredAmount);
        WaterPouring.initDesiredCapacityFromUser(asker);
        Vertices initialState = createVertices(0, 0);

        try {

            List<Vertices> expectedPath = asList(
                    initialState,
                    createVertices(0, 3),
                    createVertices(3, 0),
                    createVertices(3, 3),
                    createVertices(5, 1));

            pathFinding(initialState);
            assertEquals(expectedPath, WaterPouring.getShortestPathToTarget());

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_9_7_bottles_4_liters_desired() {
        firstBottleCapacity = 9;
        secondBottleCapacity = 7;
        desiredAmount = 4;

        when(asker.ask(WaterPouring.ENTER_FIRST_BOTTLE_VOL)).thenReturn(firstBottleCapacity);
        when(asker.ask(WaterPouring.ENTER_SECOND_BOTTLE_VOL)).thenReturn(secondBottleCapacity);
        when(asker.ask(WaterPouring.ENTER_DESIRED_TARGET_AMOUNT)).thenReturn(desiredAmount);
        WaterPouring.initDesiredCapacityFromUser(asker);
        Vertices initialState = createVertices(0, 0);

        List<Vertices> expectedPath = asList(
                initialState,
                createVertices(9, 0),
                createVertices(2, 7),
                createVertices(2, 0),
                createVertices(0, 2),
                createVertices(9, 2),
                createVertices(4, 7));

        pathFinding(initialState);
        assertEquals(expectedPath, WaterPouring.getShortestPathToTarget());
    }

    private void pathFinding(final Vertices initialState) {
        WaterPouring.createStatesGraph();
        WaterPouring.initAdjacency();
        WaterPouring.computePaths(vertices.get(vertices.indexOf(initialState)));
    }

    private Vertices createVertices(final int volX, final int volY) {
        return new Vertices(volX, volY, firstBottleCapacity, secondBottleCapacity);
    }
}
