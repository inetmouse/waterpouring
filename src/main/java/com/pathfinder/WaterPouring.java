package com.pathfinder;

import com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.*;

import static com.pathfinder.Operation.*;

public class WaterPouring {
    public static final String ENTER_FIRST_BOTTLE_VOL = "Enter bottle volume:";
    public static final String ENTER_SECOND_BOTTLE_VOL = "Enter another bottle volume:";
    public static final String ENTER_DESIRED_TARGET_AMOUNT = "Enter desired target amount:";
    private static int desiredTargetAmount;
    static List<Vertices> vertices;
    static boolean[][] adj;
    static int firstJarVol;
    static int secondJarVol;

    public static void computePaths(Vertices source) {
        source.setMinDistance(0);
        PriorityQueue<Vertices> verticesQueue = new PriorityQueue<Vertices>();
        verticesQueue.add(source);

        while (!verticesQueue.isEmpty()) {
            Vertices u = verticesQueue.poll();

            for (Vertices ver : u.getAdjacency()) {
                double distanceThroughU = u.getMinDistance() + 1;
                if (distanceThroughU < ver.getMinDistance()) {
                    verticesQueue.remove(u);

                    ver.setMinDistance(distanceThroughU);
                    ver.setPrevious(u);
                    verticesQueue.add(ver);
                }
            }
        }
    }

    public static List<Vertices> getShortestPathTo(Vertices target) {
        List<Vertices> path = new ArrayList<Vertices>();
        for (Vertices vertex = target; vertex != null; vertex = vertex.getPrevious()) {
            path.add(vertex);
        }

        Collections.reverse(path);
        return path;
    }

    public static void createStatesGraph() {
        List<Vertices> verticesList = new ArrayList();

        for (int i = 0; i < firstJarVol; i++) {
            verticesList.add(new Vertices(i, 0, firstJarVol, secondJarVol));
        }

        for (int i = 0; i <= secondJarVol; i++) {
            verticesList.add(new Vertices(firstJarVol, i, firstJarVol, secondJarVol));
        }

        for (int i = 0; i < firstJarVol; i++) {
            verticesList.add(new Vertices(i, secondJarVol, firstJarVol, secondJarVol));
        }

        for (int i = 1; i < secondJarVol; i++) {
            verticesList.add(new Vertices(0, i, firstJarVol, secondJarVol));
        }

        vertices = ImmutableList.copyOf(verticesList);

        adj = new boolean[vertices.size()][vertices.size()];

        for (int i = 0; i < vertices.size(); i++) {
            for (int j = 0; j < vertices.size(); j++) {
                adj[i][j] = vertices.get(i).isConnected(vertices.get(j));
            }
        }
    }

    public static void initAdjacency() {
        for (int i = 0; i < vertices.size(); i++) {
            List<Vertices> connected = new ArrayList<Vertices>();
            for (int j = 0; j < vertices.size(); j++) {
                if (adj[i][j] == true) {
                    connected.add(vertices.get(j));
                }
            }
            if (!connected.isEmpty()) {
                vertices.get(i).setAdjacency(connected);
            }
        }
    }

    public static void initDesiredCapacityFromUser(ParamsAsker asker) throws NumberFormatException {
        int first = asker.ask(ENTER_FIRST_BOTTLE_VOL);
        int second = asker.ask(ENTER_SECOND_BOTTLE_VOL);

        firstJarVol = Math.max(first, second);
        secondJarVol = Math.min(first, second);
        desiredTargetAmount = asker.ask(ENTER_DESIRED_TARGET_AMOUNT);

        if(firstJarVol <= 0 || secondJarVol <= 0 || desiredTargetAmount <= 0) {
            throw new RuntimeException("All amounts should be greater then 0!");
        }
    }

    public static List<Vertices> getShortestPathToTarget() throws RuntimeException{
        List<Vertices> acceptableTargets = new ArrayList<Vertices>();
        for (Vertices v : vertices) {
            if (v.getVolX() == desiredTargetAmount || v.getVolY() == desiredTargetAmount) {
                acceptableTargets.add(v);
            }
        }

        if (acceptableTargets.isEmpty()) {
            throw new RuntimeException("Desired amount of water cannot be measured!");
        }

        Collections.sort(acceptableTargets);

        List<Vertices> path = getShortestPathTo(acceptableTargets.get(0));
        if (acceptableTargets.get(0).getPrevious() == null) {
            throw new RuntimeException("Desired amount of water cannot be measured!");
        }

        return path;
    }

    public static void main(String[] args) throws IOException {
        try {
            initDesiredCapacityFromUser(new ParamsAsker(System.in, System.out));

        } catch (NumberFormatException nfe) {
            System.err.println("Invalid Format!");
        }

        createStatesGraph();
        initAdjacency();

        Vertices initialState = new Vertices(0, 0, firstJarVol, secondJarVol);

        computePaths(vertices.get(vertices.indexOf(initialState)));

        List<Vertices> shortestPath = getShortestPathToTarget();

        Map<Vertices, Operation> instruction = new LinkedHashMap<Vertices, Operation>();

        for(int i = 0; i < shortestPath.size() - 1; i++) {
            if(shortestPath.get(i+1).equals(shortestPath.get(i).fillUpX())) {
                instruction.put(shortestPath.get(i), FILL_UP_X);
            } else if (shortestPath.get(i+1).equals(shortestPath.get(i).fillUpY())) {
                instruction.put(shortestPath.get(i), FILL_UP_Y);
            } else if (shortestPath.get(i+1).equals(shortestPath.get(i).emptyX())) {
                instruction.put(shortestPath.get(i), EMPTIFY_X);
            } else if (shortestPath.get(i+1).equals(shortestPath.get(i).emptyY())) {
                instruction.put(shortestPath.get(i), EMPTIFY_Y);
            } else if (shortestPath.get(i+1).equals(shortestPath.get(i).pourXY())) {
                instruction.put(shortestPath.get(i), POUR_XY);
            } else if (shortestPath.get(i+1).equals(shortestPath.get(i).pourYX())) {
                instruction.put(shortestPath.get(i), POUR_YX);
            }
        }

        System.out.println("Volumes of the first bottle and second bottles are " + firstJarVol
                + " and " + secondJarVol + " respectively");
        System.out.println("To get " + desiredTargetAmount + " liters one should:");
        for (Map.Entry<Vertices, Operation> entry : instruction.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue().getInstruction());
        }
        System.out.println(shortestPath.get(shortestPath.size()-1));
    }

    public static class ParamsAsker {
        private final Scanner scanner;
        private final PrintStream out;

        public ParamsAsker(InputStream in, PrintStream out) {
            scanner = new Scanner(in);
            this.out = out;
        }

        public int ask(String message) {
            out.println(message);
            return scanner.nextInt();
        }
    }
}
